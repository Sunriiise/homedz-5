$(document).ready(function(){

    var owl = $("#owlCarouselWithArrows");

  $(".owl-carousel-1").owlCarousel({
    loop:false, //Зацикливаем слайдер
                margin:20, //Отступ от элемента справа в 20px
                nav:true, //Отключение навигации
                navText : ["<img src='http://s013.radikal.ru/i325/1708/c3/ad8b3ce7407c.png'>", "<img src='http://s013.radikal.ru/i323/1708/e8/a50e10e9f4e6.png'>"],
                smartSpeed:1000, //Время движения слайда
                navElement: 'div',
                navConteinerClass: 'owl-nav',
                navClass: ['owl-prev','owl-next'],
                slideBy: 1,
                dotClass: 'owl-dot',
                dotsClass: 'owl-dots',
                dots: false,
                dotsEach: false,
                dotsData: false,
                dotsSpeed: false,
                dotsContainer: false,
                responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.

                    0:{
                        items:3
                    },
                    400: {
                        items:4
                    },
                    800: {
                        items:6
                    },
                    1350: {
                        items:12
                    },

                                       
                }
  });

      $(".owl-carousel-arrows .next").click(function() {
        owl.trigger('owl.next');
    });
    
    $(".owl-carousel-arrows .prev").click(function() {
        owl.trigger('owl.prev');
    });

});

$(document).ready(function(){

    var owl = $("#owlCarouselWithArrows");

  $(".owl-carousel-2").owlCarousel({
    loop:false, //Зацикливаем слайдер
                margin:0, //Отступ от элемента справа в 20px
                nav:false, //Отключение навигации
                smartSpeed:200, //Время движения слайда
                navElement: 'div',
                navConteinerClass: 'owl-nav',
                navClass: ['owl-prev','owl-next'],
                slideBy: 1,
                dotClass: 'owl-dot',
                dotsClass: 'owl-dots',
                dots: false,
                dotsEach: false,
                dotsData: false,
                dotsSpeed: false,
                dotsContainer: false,
                responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.

                    0:{
                        items:1
                    },

                                       
                }
  });

});

$(document).ready(function(){

    var owl = $("#owlCarouselWithArrows");

  $(".owl-carousel-3").owlCarousel({
    loop:true, //Зацикливаем слайдер
                margin:0, //Отступ от элемента справа в 20px
                nav:true, //Отключение навигации
                navText: ["<img src='http://s019.radikal.ru/i617/1708/ae/b875b8394816.png'>", "<img src='http://s018.radikal.ru/i507/1708/47/ab5401de8571.png'>"],
                smartSpeed:200, //Время движения слайда
                navElement: 'div',
                navConteinerClass: 'owl-nav',
                navClass: ['owl-prev','owl-next'],
                slideBy: 1,
                dotClass: 'owl-dot',
                dotsClass: 'owl-dots',
                dots: false,
                dotsEach: false,
                dotsData: false,
                dotsSpeed: false,
                dotsContainer: false,
                responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.

                    0:{
                        items:1
                    },

                                       
                }
  });

        $(".owl-carousel-arrows .next").click(function() {
        owl.trigger('owl.next');
    });
    
    $(".owl-carousel-arrows .prev").click(function() {
        owl.trigger('owl.prev');
    });

});

$(document).ready(function(){

    var owl = $("#owlCarouselWithArrows");

  $(".owl-carousel-4").owlCarousel({
    loop:true, //Зацикливаем слайдер
                autoplay: true,
                autoplayTimeout: 5500,
                margin:0, //Отступ от элемента справа в 20px
                nav:false, //Отключение навигации
                smartSpeed:1000, //Время движения слайда
                navElement: 'div',
                navConteinerClass: 'owl-nav',
                navClass: ['owl-prev','owl-next'],
                slideBy: 1,
                dotClass: 'owl-dot',
                dotsClass: 'owl-dots',
                dots: true,
                dotsEach: false,
                dotsData: false,
                dotsSpeed: false,
                dotsContainer: false,
                responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.

                    0:{
                        items:1
                    },

                                       
                }
  });

        $(".owl-carousel-arrows .next").click(function() {
        owl.trigger('owl.next');
    });
    
    $(".owl-carousel-arrows .prev").click(function() {
        owl.trigger('owl.prev');
    });

});

$(document).ready(function(){

    var owl = $("#owlCarouselWithArrows");

  $(".owl-carousel-5").owlCarousel({
    loop:true, //Зацикливаем слайдер
                autoplay: false,
                autoplayTimeout: 5500,
                margin:0, //Отступ от элемента справа в 20px
                nav:true, //Отключение навигации
                navText: ["<img src='http://s008.radikal.ru/i304/1708/fe/ad33d5eb93b3.png'>", "<img src='http://s019.radikal.ru/i608/1708/3b/4070e05d7d2b.png'>"],
                smartSpeed:1000, //Время движения слайда
                navElement: 'div',
                navConteinerClass: 'owl-nav',
                navClass: ['owl-prev','owl-next'],
                slideBy: 1,
                dotClass: 'owl-dot',
                dotsClass: 'owl-dots',
                dots: false,
                dotsEach: false,
                dotsData: false,
                dotsSpeed: false,
                dotsContainer: false,
                responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.

                    0:{
                        items:1
                    },

                                       
                }
  });

        $(".owl-carousel-arrows .next").click(function() {
        owl.trigger('owl.next');
    });
    
    $(".owl-carousel-arrows .prev").click(function() {
        owl.trigger('owl.prev');
    });

});